#!/usr/bin/python2
# coding: utf-8
""" Checking program called after core.run() 
Need parameters and file below which are returned/created by core.run() - 
	local, traveler: list of UserLocation instance
	<lid>.out
"""
import sys
from loadLocation import getAPI
import core

def check(point, local, traveler, filepath='32820.txt.out'):
	print "Start appending `check` information to %s..." %filepath
	api = getAPI()
	with open(filepath, 'a') as fh:
		# Locals
		for userLocation in local:
			user = api.user(userLocation.id)
			# Separating line
			fh.write('-' * 10 + "\n")
			if user.full_name <> '':
				fh.write(str(user.full_name) + "\n")
			else:
				fh.write(str(user.username) + "\n")
			fh.write(str(user.profile_picture) + "\n")
			fh.write("IS Local to %s\n" %str(point))
			fh.write(str(userLocation) + "\n")
		# Travellers
		for userLocation in traveler:
			user = api.user(userLocation.id)
			# Separating line
			fh.write('-' * 10 + "\n")
			if user.full_name <> '':
				fh.write(str(user.full_name) + "\n")
			else:
				fh.write(str(user.username) + "\n")
			fh.write(str(user.profile_picture) + "\n")
			fh.write("IS Traveller to %s\n" %str(point))
			fh.write(str(userLocation) + "\n")
				
def shortUsage():
	print 'Usage: python check.py <input_file>'

# AUTORUN	
reload(sys)
sys.setdefaultencoding('utf-8')

if __name__ == '__main__':
	if len(sys.argv)>1:
		location, local, traveler = core.run(sys.argv[1])
		check(location, local, traveler, core.outputFile)
		
	else:
		shortUsage()
		sys.exit(2)