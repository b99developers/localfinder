#!/usr/bin/python2
# coding: utf-8
""" Library for finder.py """

""" From: http://www.peterbe.com/plog/uniqifiers-benchmark """
def unique(seq, idfun=None): 
   # order preserving
   if idfun is None:
	   def idfun(x): return x
   seen = {}
   result = []
   for item in seq:
	   marker = idfun(item)
	   # in old Python versions:
	   # if seen.has_key(marker)
	   # but in new ones:
	   if marker in seen: continue
	   seen[marker] = 1
	   result.append(item)
   return result
		
def link2Top(text):
    return '<a title="Click to go to top" href="#Top">%s</a>' % (text)

def img(image, text=''):
	return '<img src="%s" alt="%s">' %(image, text)

def link(content, target):
	return '<a href="%s" target="_blank">%s</a>' %(target, content)
	
class HtmlBuilder():
	def __init__(self, title, header=''):
		self.title = title
		self.header = header
		buffer = []
		buffer.append("<!--" + self.header + "-->\n")
		buffer.append('''<html>
<head>
<title>%s</title>
<meta charset="utf-8"/>
</head>
<body>''' % self.title)
		self.buffer = buffer
	
	def __repr__(self):
		return "<HtmlBuilder: %s>"%self.title
	
	def __str__(self):
		return "".join(self.buffer) + "</body></html>"
	
	def append(self, new):
		try:
			self.buffer.append(str(new))
		except:
			print "Not appended"
	
	def getResult(self):
		return str(self)
	