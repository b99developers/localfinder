# Instagram Local Finder #

by CheerTour development team. Mainly coded by JohnTsai/JerJer

### Dependency ###

* geopy  
https://github.com/geopy/geopy
* python-instagram  
https://github.com/Instagram/python-instagram
* numpy  
https://www.numpy.org  


### Branches ###

* Sample  
Method developed by JerJer. Find the locations of the user at four specific period, then give out an answer of "He/She is local or not.".  
* Slice  
Method inspired by Jimmy. Slice the time line of an user. Then detect that in each time slice, the user is whether traveling or at home.  
* NoInsta
Come from slice, which split the crawling from instagram job and the others. Follow the input format to use the core functionality only.  


## Usage (of branch noInsta) ##  

0. Download the outer dependncy packages.
1. Learn the input format in file 'input.txt'.  
2. Write the location position, userid, locations of users' posts in a file following the input format.  
3. Run 'python core.py <filename>' to compute the result.  
  
Note that <filename>.out will be overwritten by core.py.  
