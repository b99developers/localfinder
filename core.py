#!/usr/bin/python2
# coding: utf-8
"""
Core function of 'Slice' method.
@ input:
	All users' post location. See 'input.txt' to learn about the format.
@ arguments:
	s = range to say that an user is at the same place.
@ strategy:
	result = classifier(groups_of_points, location_point, [s])
	
Instruction:
1. Loop: until all posts are classified in a group.
	a. Find the location of the user's latest post and claim it a "centroid". 
	b. Trace back by time, classify all locations in the range of centroid as 
		the same group.
	c. When a location not in the range is found, set it as the new centroid 
		which belongs to a new group. Go on next iteration.
2. Send all groups and the geo-code of the location into a "classifier_strategy
" to get a final result.
"""
# note - timestamp = datetime_to_timestamp(Media.created_time)

import string
import re
import sys
import time
import copy
from geopy.distance import distance
import classifier 

# Global Vars
lid = 32820			# CKS-memorial hall
s = 100 			# 100 km
outputFile = 'output.txt'

def getS():
	global s
	return copy.deepcopy(s)
	
def _getAllLocationFromMedia(medias):
	locs = []
	for media in medias:
		try:
			locs.append(media.location)
		except:
			print "%s - no location" % (media)
	return locs

""" Load from input_file and get locations.
@ return: lid, info, ul
	lid - location id
	info - Information to be shown in the result file, in type 'list of str'.
	ul - users and their locations in type 'list of UserLocation'.
"""
def loadFromFile(input_file='32820.txt'):
	global outputFile
	location = -1
	info = []
	ul = []
	outputFile = input_file + ".out"
	with open(input_file, 'r') as fh:
		for line in fh:
			if line[0] == '#':	# Comment
				continue
			elif location == -1: # First line = lid
				result = re.search(r'([0-9\.]+)\s*,\s*([0-9\.]+)', line)
				try:
					location = (result.group(1), result.group(2))
				except (AttributeError, TypeError):
					print "Input Format Error. Please refer to 'input.txt'.\n"
					raise
			elif line[0] == '_':	# info
				info.append(line[1:].strip())
			else:	# user and location
				tmp = line.split(':')
				uid = tmp[0].strip()
				positions = re.findall(r'\(([0-9\.]+)\s*,\s*([0-9\.]+)\s*\)', tmp[1])
				ul.append(UserLocation(uid, positions))
	return location, info, ul

""" main function """
def groupPoints(points):
	global s
	groups = []
	tmpGroup = []
	centroid = None 
	for p in points:
		try:
			# Initialize
			if centroid == None:
				tmpGroup = [p]
				centroid = p
			# New group
			elif distance(p, centroid).km > s:
				groups.append(tmpGroup)
				centroid = p
				tmpGroup = [p]
			# Append to previous group
			else:
				tmpGroup.append(p)
		except AttributeError:
			"""No location"""
			continue
	# Terminating
	groups.append(tmpGroup)
	return groups
	
""" Old version """
def groupMedias(medias):
	global s
	groups = []
	tmpGroup = []
	centroid = None 
	for media in medias:
		try:
			# Initialize
			if centroid == None:
				tmpGroup = [media]
				centroid = media.location
			# New group
			elif locationDistance(media.location, centroid) > s:
				groups.append(tmpGroup)
				centroid = media.location
				tmpGroup = [media]
			# Append to previous group
			else:
				tmpGroup.append(media)
		except AttributeError:
			"""No location"""
			continue
	# Terminating
	groups.append(tmpGroup)
	return groups
	
def locationDistance(loc1, loc2):
	p1 = (loc1.point.latitude, loc1.point.longitude)
	p2 = (loc2.point.latitude, loc2.point.longitude)
	return distance(p1, p2).km
	
class UserLocation():

	def __init__(self, id, locations=None):
		self.id = id		
		if locations == None:
			self.locations = []
		else:
			self.setLocations(locations)
			
	def __repr__(self):
		return "<UserLocation: %s>" %self.id

	def __str__(self):
		sb = []
		sb.append("UserID: %s - " %self.id)
		for x in self.locations:
			sb.append("%s, " %str(x))
		return "".join(sb)
		
	def setLocations(self, locations):
		try:
			self.locations = list(locations)
		except TypeError:
			print "TypeError: The argument must be a set or a list.\n"

	def len(self):
		return len(self.locations)
	
def run(inputFile='32820.txt'):
	global outputFile
	location, info, ul = loadFromFile(inputFile)
	local = []
	traveler = []
	method = classifier.getDefault()
	# Main computation
	for user in ul:
		print "Working on " + repr(user)
		# Step 1. Group all of the points.
		groups = groupPoints(user.locations)
		# Step 2. Apply classifier strategy on the groups to get the final 
		# result
		if method(groups, location):	# is local
			local.append(user)
		else:
			traveler.append(user)
	localRatio = float(len(local)) / (len(local) + len(traveler))
	# Print the result into output file
	with open(outputFile ,'w') as fh:
		fh.write("# " + "\n# ".join(info) + "\n")
		fh.write("Local ratio: %s\n" %str(localRatio))
		fh.write("Traveller ratio: %s\n" %str(1 - localRatio))
	print "Done."
	return location, local, traveler
	
def shortUsage():
	print 'Usage: python core.py <input_file>'

# AUTORUN	
reload(sys)
sys.setdefaultencoding('utf-8')

if __name__ == '__main__':
	if len(sys.argv)>1:
		run(sys.argv[1])
		
	else:
		shortUsage()
		sys.exit(2)