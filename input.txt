# Input.txt
# This is the sample input file format for local finder, 'noInsta' branch.
# All lines with '#' as the first character will be regarded as comment and be ignored.
#
# The first line must contain the location position (in tuple).
25.03541084, 121.520140171
# 
# The lines with '_' as the first character contains the information to be shown on the top of the result.
# 
_Name: 中正紀念堂 Chiang Kai-Shek Memorial Hall
_Point: (25.03541084, 121.520140171)
_ID: 32820
#
# Other lines are user location. 
# Put uid (user id) before ':', and all position of the user in one line, split by comma (,).
# Split each line by '\n'.
# uid: (0,1),(2,3),...
# Note that spaces beside the comma are ignored.
#
43518852 : (23.139548, 116.412758), (22.350681, 113.526341), (22.301534, 113.553295), (22.350232, 113.526307), 
362829316 : (25.042576, 121.525868), (25.068423, 121.529876), (25.078861, 121.538422), (25.090963, 121.546694), 
398499419 : (24.647282, 121.245961), (23.850939, 121.436998), (24.738605, 121.299715), (24.225528, 120.894516), 
