""" Determines 
prototype: result = determine(filtered_list, **kwargs)

Determines should:
	1. Return printable, readable result.
	2. Specify input format.
"""
import time
from numpy import linalg as LA
from geopy.distance import distance

def returnAllDetermine(points, **kwargs):
	return points

""" For text or Boolean input. Return item in input_list with the highest 
count.
"""
def findMostDetermine(input_, **kwargs):
	count = {}
	try:
		if len(input_) > 1:
			for x in input_:
				if x not in count.keys():
					count.update({x: 1})
				else:
					count[x] += 1
			output = sorted(count, key=count.get)
			#print output
			return output[-1]
		elif len(input_) == 1:
			return input_[0]
	except TypeError:
		print "Warning: Input argument not iterable."
		return input_
	
def findCentroidDetermine(points, **kwargs):
	startTime = time.time()
	minVar = float('inf')
	centroid = None
	
	try:
		if type(points[0]) is not tuple:
			print "Not tuple"
			raise TypeError
	except TypeError:
		print "Input must be list of geocode tuples"
	for x in points:
		dist = []
		for y in points:
			dist.append(distance(x,y).km)
		norm = LA.norm(dist)
		if norm < minVar:
			minVar = norm
			centroid = x
	endTime = time.time()
	print "findCentroidDetermine done. Cost %s seconds" %(endTime - startTime)
	return centroid
	
""" Compute the distance between centroid of points and location """
def calcDistDetermine(points, **kwargs):
	if 'location' not in kwargs.keys():
		raise TypeError("keyword arg 'location' must be set")
	else:
		print "# LOCATION:" + str(kwargs['location'])

	centroid = findCentroidDetermine(points)
	results = []
	results.append(str(centroid))
	results.append(str(distance(centroid, kwargs['location']).km) + " km")
	return "    ".join(results)
	
def getDefaultDetermine():
	return None