#!/usr/bin/python2
# coding: utf-8
"""
Load data using instagram API and generate an input file.
"""

import random
import string
import re
import sys
import time
import copy
import getopt
from instagram import client, oauth2, InstagramAPIError, InstagramAPI
from instagram.helper import datetime_to_timestamp, timestamp_to_datetime

# Global Vars
lid = 32820			# CKS-memorial hall

def __initAPI():
	global __api
	global __access_token_list
	__access_token_index = random.randrange(len(__access_token_list))
	__api = InstagramAPI(access_token=__access_token_list[__access_token_index])
	#__api = InstagramAPI(access_token=__my_access_token)
	return __api

""" Return API instance. Generate one if needed. """
def getAPI():
	global __api
	if __api <> None:
		return __api
	else:
		return __initAPI()
	
""" Call if you doesn't know any location_id to start the main computation """
def getLocationFromFeed(count=1):
	__api = getAPI()
	locs = []
	max_ID = -1

	while len(locs) < count:
		dump = None
		if max_ID == -1:
			dump = __api.user_media_feed(count=10)
		else:
			dump = __api.user_media_feed(count=10, max_id= max_ID)
		feeds = dump[0]
		url = dump[1]
		max_ID = getMaxID(url)
		
		locations = _getAllLocationFromMedia(feeds)
		# Find those with name and id
		for loc in locations:
			try:
				if loc.name:
					if loc.id <> 0 and loc not in locs:
						locs.append(loc)
					else:
						print "> Location without id"
			except:
				print loc
				print "> No location"
	print "%s locations found." %len(locs)
	return locs
	
""" Find users who have posted at the location. """
def getUserInLocation(location_id=lid, count=10, max_id=-1):
	global locationMaxID
	__api = getAPI()

	user_list = []
	while(count == -1 or len(user_list) < count):
		if max_id == -1:
			dump = __api.location_recent_media(count=10, location_id=location_id)
		else:
			dump = __api.location_recent_media(count=10, location_id=location_id, max_id=max_id)
		medias = dump[0]
		url = dump[1]
		max_id = getMaxID(url)
		if max_id <> -1:
			print "return max_id : " + max_id
		else:
			print "No other results"
			break
		for med in medias:
			user_list.append(med.user)
	print user_list
	return user_list

""" Get the next max_id from url return by api
	return -1 when there's no other results
"""
def getMaxID(url):
	pattern = re.compile("max_id=([0-9\_]+)")
	try:
		get_max_id = re.search(pattern, url).groups()[0]
	except:	
		get_max_id = -1
	return get_max_id
	
""" Get all post with location of an user. """
def getAllUserPost(userid, count=-1, timestamp=None):
	medias = []
	hasLocation = []
	nextID = 0		# 0 as start, -1 as stop
	__api = getAPI()
		
	while nextID <> -1:
		kwargs = {'user_id':userid, 'count':10}
		if timestamp <> None:
			kwargs.update({'max_timestamp':timestamp})
		if nextID <> 0 and nextID <> -1:
			kwargs.update({'max_id':nextID})
		for x, y in kwargs.iteritems():
			print "%s -> %s" %(x, y)
		dump = __api.user_recent_media(**kwargs)
		medias += dump[0]
		url = dump[1]
		nextID = getMaxID(url)
		if nextID <> -1:
			print "return max_id : " + nextID
		else:
			print "No other results"
	# Remove those without location
	for media in medias:
		if hasattr(media, 'location'):
			hasLocation.append(media)
		else:
			print "Removed", media
	return hasLocation
		
# timestamp = datetime_to_timestamp(Media.created_time)

def _getAllLocationFromMedia(medias):
	locs = []
	for media in medias:
		try:
			locs.append(media.location)
		except:
			print "%s - no location" % (media)
	return locs
	
def getAllPointFromMedia(medias):
	points = []
	for media in medias:
		try:
			points.append((media.location.point.latitude, media.location.point.longitude))
		except:
			print "%s - no location" % (media)
	return points

def locationDistance(loc1, loc2):
	p1 = (loc1.point.latitude, loc1.point.longitude)
	p2 = (loc2.point.latitude, loc2.point.longitude)
	return distance(p1, p2).km
	
class UserLocation():

	def __init__(self, id, locations=None):
		self.id = id		
		if locations == None:
			self.locations = []
		else:
			self.setLocations(locations)
			
	def __repr__(self):
		return "<UserLocation: %s>" %self.id

	def __str__(self):
		sb = []
		sb.append("UserID: %s - " %self.id)
		for x,y in self.locations:
			sb.append("(%f, %f), " %(x, y))
		return "".join(sb)
		
	def setLocations(self, locations):
		try:
			self.locations = list(locations)
		except TypeError:
			print "TypeError: The argument must be a set or a list.\n"

	def len(self):
		return len(self.locations)
	
def run(location_id, count=100):
	global lid
	startTime = time.time()
	api = getAPI()
	outputStrings = []
	
	# Super mode. Just use the default lid
	if str(location_id) == '-1':
		print "Use default location."
		location_id = lid
	outputFile = '%s.txt' %str(location_id)
	
	location = api.location(location_id)
	# position of the location 
	outputStrings.append(str((location.point.latitude, location.point.longitude)))
	# Optional information
	outputStrings.append("_ID: %s" %str(location.id))
	outputStrings.append("_Name: %s" %str(location.name))
	outputStrings.append('_' + str(location.point))
	
	users = getUserInLocation(location_id=location_id, count=count)
	for user in users:
		posts = getAllUserPost(user.id)
		points = getAllPointFromMedia(posts)
		tmp = []
		for p in points:
			tmp.append(str(p))
		outputStrings.append(str(user.id) + ':' + ', '.join(tmp))
	with open(outputFile, 'w') as fh:
		fh.write("\n".join(outputStrings))
	endTime = time.time()
	print "Run (lid=%s) done in %d secs." %(str(location_id), endTime - startTime)
	
def shortUsage():
	print 'Usage: python loadLocation.py [location_id]'

# AUTORUN	
__access_token_list = ['1481931227.5b9e1e6.793bd1f0c89e4d1d91a4d1adf5394008']
__my_access_token = '1419185731.0772f7f.1fd1460ee0eb411bafa8213ea41bf722'
locationMaxID = -1
reload(sys)
sys.setdefaultencoding('utf-8')
__api = __initAPI()

if __name__ == '__main__':
	if len(sys.argv) == 2:
		run(sys.argv[1])
	else:
		shortUsage()
		sys.exit(2)